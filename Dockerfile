FROM debian:jessie

LABEL name="Debian 8 Base Image (Spanish)" \
    maintainer="j.dasua@gmail.com"

## Install base packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get --force-yes upgrade && \
    apt-get --force-yes dist-upgrade && \
    apt-get -y install apt-utils bash-completion locales tzdata less nano && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archive/*.deb && \
    localedef -i es_ES -c -f UTF-8 -A /usr/share/locale/locale.alias es_ES.UTF-8
ENV LANG es_ES.utf8

RUN rm /etc/localtime
RUN echo "Atlantic/Canary" > /etc/timezone && ln -s /usr/share/zoneinfo/Atlantic/Canary /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

